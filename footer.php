<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 footer-mpm">
					<div class="mpm-logo">
						<img src="assets/images/mpm2.jpg">
					</div>
					<div class="mpm-desc">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
					</div>
					<div class="mpm-social">
						<ul class="social">
                            <li>
                            	<a class="social-box">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
                            <li>
                            	<a class="social-box">
									<i class="fa fa-linkedin" aria-hidden="true"></i>
								</a>
                            </li>
                            <li>
	                            <a class="social-box">
									<i class="fa fa-instagram" aria-hidden="true"></i>
								</a>
                            </li>
                            <li>
	                           <a class="social-box">
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
                            </li>
                        </ul>
						
					</div>
				</div>
				<div class="col-sm-4 footer-menu" style="background: black;">
					<div class="col-sm-2"></div>
					<div class="footermenu-item col-sm-4 nopadding">
						<div class="footermenu-title">
							<img src="assets/images/icon-footer.jpg">
							<hr>
						</div>
						<ul class="nav nav-stacked nav-footer ">
		                    <li><a href="#">Honda Matic</a></li> 
		                    <li><a href="#">Honda Bebek</a></li> 
		                    <li><a href="#">Honda Sport</a></li> 
		                </ul>
					</div>
					<div class="col-sm-1"></div>
					<div class="footermenu-item col-sm-4 nopadding">
						<div class="footermenu-title">
							<img src="assets/images/icon-footer.jpg">
							<hr>
						</div>
						<ul class="nav nav-stacked nav-footer ">
		                    <li><a href="#">Info</a></li> 
		                    <li><a href="#">Technology</a></li> 
		                    <li><a href="#">Safety Riding</a></li> 
		                    <li><a href="#">Moto GP</a></li> 
		                </ul>
					</div>
					<div class="col-sm-1"></div>
				</div><!--END FOOTER MENU-->
				<div class="col-sm-5 footer-location">
					<div class="location-top">
						<div class="location-row">
							<div>
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div>
								Jl. Simpang Dukuh No. 42-44, Genteng,</br>
								Surabaya City, East Java 60275
							</div>
							<hr>
						</div><!--END LOCATION ROW-->
						<div class="location-row">
							<div>
								<i class="fa fa-clock-o" aria-hidden="true"></i>
							</div>
							<div>
								9:00am-5:00pm</br>
								Sunday Closed
							</div>
							<hr>
						</div><!--END LOCATION ROW-->
					</div><!--END Location top-->
					<div class="location-top contact-footer">
						<div class="contact-row">
							<div class="contact-box">
								<div class="col-xs-3 nopadding">
									<img src="assets/images/logo-honda.jpg">
								</div>
								<div class="col-xs-9">
									<div class="customer-care">
										CUSTOMER CARE:
										</br>
										<div class="cust-call">
											<span><i class="fa fa-phone-square" aria-hidden="true"></i></span>
											<span>0800-11-46632</span>
										</div>
									</div>
									<div class="email-footer">
										EMAIL:</br>
										customercare@mpm_id.com
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div><!--END CONTACT TOP-->
				</div><!--END FOOTER LOCATION-->
			</div>
		</div>
	</div><!--END FOOTER TOP-->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="pull-left">
					Copyright 2017, PT Mitra Pinasthika Mulia. All Right Reserved
				</div>

				<!-- backtop -->
			    <div class="go-up">
			        <a href="#"><i class="fa fa-angle-up"></i></a>    
			    </div><!-- end backtop -->
			</div>
		</div>
	</div><!--END COPYRIGHT-->


</footer>