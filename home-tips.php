<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" class=" td-md-is-chrome js_active  vc_desktop  vc_transform  vc_transform ">
<!--<![endif]-->
<head>
	<title>Honda</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" id="honda-css" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="honda-css" href="assets/css/style-yvonne.css" type="text/css" media="all">
</head>
<body>
	<?php include("header.php");?>
    <section class="video-banner">

    </section>
    <section class="home-tips">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-tips">
                    <div class="tips-box">
                        <!-- <img src="assets/images/tips.jpg"> -->
                        <div class="black" style="background-color: #000;height:300px;"><img src="assets/images/tips.jpg"></div>
                        <div class="caption">
                            <div class="date">13 April 2017</div>
                            <div class="title">MXGP Meksiko: Akhir Pekan </br>Sempurna Buat Tim Gajser</div>
                            <div class="content">Crosser tim Honda Racing Corporation, Tim Gajser, berhasil mempertahankan
posisi teratas di klasemen sementara MXGP setelah meraih kemenangan
keduanya musim ini di seri MXGP Meksiko akhir pekan (3/4) lalu.</div>
                            <div class="read-more"><a href="#">READ MORE...</a>
                        </div>
                    </div>
                    <!--END TIPS BOX-->
                </div>
            </div>
        </div>
    </section>
	<?php include("footer.php");?>
	 <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>