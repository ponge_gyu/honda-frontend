<header>
	<div class="top-menu">
		<div class="container">
			<div class="row">
				<ul class="nav nav-wrap">
				  <li role="presentation"><a href="#">Dealer</a></li>
				  <li role="presentation"><a href="#">Contact Us</a></li>
				  <li>
					  	<a class="dropdown-toggle searchbutton" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
	                    <ul class="search-dropdown">
	                        <form>
	                            <div class="input-group search-inputgroup">
	                                <input id="search2" type="search" value="" class="form-control" placeholder="type keyword(s) here" />
	                                <span class="btn input-group-addon "><i class="fa fa-search"></i></span>                              
	                            </div>
	                        </form>
	                    </ul> 
	                </li>
				</ul>
			</div>
		</div>	
	</div><!--END TOP MENU-->

	<div class="header-menu_wrapper">
		<div class="container">
			<div class="row">
				<div class="header-menu">
					<div class="logo-left">
						<img src="assets/images/mpm.jpg">
					</div>
					<div class="navmain_wrapper">
						<div class="navbar-header">
		                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav">
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>
		                </div>
		                <div class="collapse navbar-collapse main-nav">
		                	<ul class="nav nav-wrap nav-main">
							  <li role="presentation" class="active"><a href="#">Home</a></li>
							  <li role="presentation"><a href="#">Corporate</a></li>
							  <li role="presentation"><a href="#">Products</a></li>
							  <li role="presentation"><a href="#">Price list</a></li>
							  <li role="presentation"><a href="#">Network</a></li>
							  <li role="presentation"><a href="#">Career</a></li>
							  <li role="presentation"><a href="#">News</a></li>
							</ul>
		                </div>
					</div>
					<div class="logo-right">
						<img src="assets/images/honda.jpg">
					</div>
				</div><!--END HEADER MENU-->
				
			</div>
		</div>
		
	</div><!--END MAIN MENU-->

</header>