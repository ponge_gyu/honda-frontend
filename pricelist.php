<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" class=" td-md-is-chrome js_active  vc_desktop  vc_transform  vc_transform ">
<!--<![endif]-->
<head>
	<title>Honda</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" id="honda-css" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="honda-css" href="assets/css/style-yvonne.css" type="text/css" media="all">
</head>
<body>
	<?php include("header.php");?>
    <section class="pricelist">
        <div class="container">
            <div class="pricelisttable_wrapper">
                <div class="pricelist-title">Daftar harga OTR Surabaya</div>
                <div class="hargakotalain">
                    <div class="btn-group">
                        <div class="group-title">HARGA KOTA LAINNYA</div>
                          <select class="group-select">
                            <option value="" disabled selected>Pilih Kota/daerah</option>
                            <option value="france" data-class="flag-france">France</option>
                            <option value="brazil" data-class="flag-brazil">Brazil</option>
                            <option value="argentina" data-class="flag-argentina">Argentina</option>
                            <option value="south-africa" data-class="flag-safrica">South Africa</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-pricelist">
                        <thead>
                            <tr>
                                <th>TIPE MOTOR</th>
                                <th>HARGA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                            <tr>
                                <td>SUPRA X FI</td>
                                <td>17,250,000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
	<?php include("footer.php");?>
	 <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>