$(document).on('click', '.searchbutton', function(){ 
    $(".search-dropdown").slideToggle(500);
});

function go_up(){

    $('.go-up a').on('click', function (e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
}


$(document).ready(function(){ 
    go_up();
});