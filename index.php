<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" class=" td-md-is-chrome js_active  vc_desktop  vc_transform  vc_transform ">
<!--<![endif]-->
<head>
	<title>Honda</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" id="honda-css" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="honda-css" href="assets/css/style-yvonne.css" type="text/css" media="all">
</head>
<body>
	<?php include("header.php");?>
    <section class="main-content" style="height:1000px;">
    </section>
	<?php include("footer.php");?>
	 <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>