<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" class=" td-md-is-chrome js_active  vc_desktop  vc_transform  vc_transform ">
<!--<![endif]-->
<head>
	<title>Honda</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" id="honda-css" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="honda-css" href="assets/css/style-yvonne.css" type="text/css" media="all">
</head>
<body>
	<?php include("header.php");?>
    <section class="career-banner">
        <div class="career-topbanner" style="background-image:url('assets/images/career.jpg');">
            <div class="career-join">
                <div class="career-white">
                    JOIN OUR
                </div>
                <div class="career-orange">
                    WONDERFUL TEAM
                </div>
                <div class="career-btnjoin">
                    <a href="#" class="btn btnjoin">NOW</a>
                </div>
            </div>
        </div>
    </section>
    <section class="career-list">
            <div class="career-listblack">
                <div class="container">
                    <div class="back">01</div>
                    <div class="front">Lowongan Kerja <hr></div>
                </div>
            </div>
            <div class="career-listwhite">
                 <div class="container">
                    <div class="career-box_wrapper">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="career-box">
                                    <p><img src="assets/images/career-icon.png"></p>
                                    <p class="title">
                                        <span class="orange">BRANCH HEAD</span> - JM-BHE
                                    </p>
                                    <p class="job-decs">
                                        Responsible for branches, especially in of target sales, operating and human resources.
                                    </p>
                                    <div class="qualification">
                                        <ul>
                                            <li>Male</li>
                                            <li>Maximum Age: 35 years</li>                                            
                                            <li>Bachelor degree for all majors</li>
                                            <li>Have experience in automotive
                                            and leading a team</li>                                            
                                            <li>Work location: willing to be placed in MPM operational areas (East Java & East Nusa Tenggara)</li>
                                        </ul>
                                    </div>
                                </div><!--END CAREER BOX-->
                            </div><!--END COL CAREER-->
                            <div class="col-sm-4">
                                <div class="career-box">
                                    <p><img src="assets/images/career-icon2.png"></p>
                                    <p class="title">
                                        <span class="orange">SALESMAN</span> - JM-SLS
                                    </p>
                                    <p class="job-decs">
                                        Responsible for branches, especially in of target sales, operating and human resources.
                                    </p>
                                    <div class="qualification">
                                        <ul>
                                            <li>Male</li>
                                            <li>Maximum Age: 35 years</li>                                            
                                            <li>Bachelor degree for all majors</li>
                                            <li>Have experience in automotive
                                            and leading a team</li>                                            
                                            <li>Work location: willing to be placed in MPM operational areas (East Java & East Nusa Tenggara)</li>
                                        </ul>
                                    </div>
                                </div><!--END CAREER BOX-->
                            </div><!--END COL CAREER-->
                            <div class="col-sm-4">
                                <div class="career-box">
                                    <p><img src="assets/images/career-icon2.png"></p>
                                    <p class="title">
                                        <span class="orange">SALESMAN</span> - JM-SLS
                                    </p>
                                    <p class="job-decs">
                                        Responsible for branches, especially in of target sales, operating and human resources.
                                    </p>
                                    <div class="qualification">
                                        <ul>
                                            <li>Male</li>
                                            <li>Maximum Age: 35 years</li>                                            
                                            <li>Bachelor degree for all majors</li>
                                            <li>Have experience in automotive
                                            and leading a team</li>                                            
                                            <li>Work location: willing to be placed in MPM operational areas (East Java & East Nusa Tenggara)</li>
                                        </ul>
                                    </div>
                                </div><!--END CAREER BOX-->
                            </div><!--END COL CAREER-->

                        </div>
                    </div>
                    <div class="career-register">
                        <a href="#" class="btn btn-career">Please register to our website: recruitment.mpm-motor.com</a>
                    </div>
                 </div>
            </div>
    </section>
	<?php include("footer.php");?>
	 <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>