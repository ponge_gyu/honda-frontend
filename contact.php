<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" class=" td-md-is-chrome js_active  vc_desktop  vc_transform  vc_transform ">
<!--<![endif]-->
<head>
	<title>Honda</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" id="honda-css" href="assets/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="honda-css" href="assets/css/style-yvonne.css" type="text/css" media="all">
</head>
<body>
	<?php include("header.php");?>
    <section class="contact-map" style="height:500px;">

    </section>
    <section class="contact-us">
        <div class="content">
            <div class="title">
                Contact Us
                <hr>
            </div>
            <p class="sub-title">
                Contrary to popular belief, Lorem Ipsum is not simply random text.
                It has roots in a piece of classical from 45 BC making it over 2000 years old
            </p>
            <div class="contact-form">
                <form>
                    <div class="col-md-6">
                       <div class="form-group">
                            <input type="text" class="form-control" id="contact-name" placeholder="Your Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                            <input type="email" class="form-control" id="contact-email" placeholder="Your Email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="contact-subject" placeholder="Subject">
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Write your message...</label>
                            <textarea class="form-control" id="contact-message" row="5"></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-contact">Send Message</button>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
	<?php include("footer.php");?>
	 <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>